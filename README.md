# Weather app on fragments
---
1. Choose a city from the list. List is spinnable
2. Tap on the chosen city
If the city image is available it will be loaded in a few seconds 
---

Application just after start
![start](./.raw/start.png)

After loading the weather
![loaded](./.raw/data_loaded.png)


City image
![cityImage](./.raw/phoenix.png)

No city Image
![noCityImage](./.raw/no_hoto.png)