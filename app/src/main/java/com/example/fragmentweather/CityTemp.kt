package com.example.fragmentweather

import com.google.gson.annotations.SerializedName

data class CityTemp(
    @SerializedName("name")
    val name: String? = null,

    @SerializedName("temp")
    var temp: Float? = null,

    @SerializedName("windSpeed")
    var windSpeed: Float = -1f,
    )