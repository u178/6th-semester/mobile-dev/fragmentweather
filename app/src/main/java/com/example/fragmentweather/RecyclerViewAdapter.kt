package com.example.fragmentweather

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter(private var cityList: ArrayList<CityTemp>) : RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder>() {

    private lateinit var itemListener: onItemClickListener
    interface onItemClickListener   {

        fun onItemClick(position: Int) {

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return RecyclerViewHolder(itemView, itemListener)
    }

//    fun update(modelList:ArrayList<CityTemp){
//        cityList = modelList
//        Rec!!.notifyDataSetChanged()
//    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val currentItem = cityList[position]
        holder.cityName.text = currentItem.name
        holder.cityTemp.text = currentItem.temp?.toInt().toString() + " ℃"
    }

    override fun getItemCount(): Int {
        return cityList.size
    }

    fun setOnItemClickListener(listener: onItemClickListener) {
        this.itemListener = listener
    }

    class RecyclerViewHolder(itemView: View, listener: onItemClickListener) : RecyclerView.ViewHolder(itemView){
        val cityName: TextView = itemView.findViewById(R.id.city)
        val cityTemp: TextView = itemView.findViewById(R.id.temp)


        init {
            itemView.setOnClickListener {
                listener.onItemClick(adapterPosition)
            }
        }

    }
}