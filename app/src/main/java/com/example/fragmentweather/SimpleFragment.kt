package com.example.fragmentweather

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.currentweatherdatabinding.Cities
import com.google.gson.Gson
import com.google.gson.JsonParser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SimpleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SimpleFragment : Fragment() {
    private lateinit var newRecyclerView: RecyclerView
    private lateinit var newArrayList: ArrayList<CityTemp>
    lateinit var cityName: Array<String>
    lateinit var cityTemp: Array<Float>
    private lateinit var gson: Gson
    private lateinit var citiesArray: Array<City>
    private var API_KEY = ""
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        API_KEY = getString(R.string.API_KEY)
        arguments?.let {
        }
        gson = Gson()
        loadCities()
//        cityName = arrayOf("Gniezno", "Irkutsk", "Poznan", "Warsaw", "Berlin", "London")
//        cityTemp = arrayOf(10f, 2f, 8f, 8f, 7f, 6f)



    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_simple, container, false)
        newRecyclerView = view.findViewById(R.id.recyclerView)
        newRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        newRecyclerView.setHasFixedSize(true)

        newArrayList = arrayListOf<CityTemp>()
        citiesArray = loadCities()
//        GlobalScope.launch (Dispatchers.IO) {
//            getCityData()
//        }


        for (c in citiesArray) {

            var tempAndWind = Pair(0f, 0f)
            val cityTemp = CityTemp(c.capital, tempAndWind.first, tempAndWind.second)
            newArrayList.add(cityTemp)
//            }


            getCityData()
        }
        for (i in citiesArray.indices) {
            GlobalScope.launch (Dispatchers.IO) {
                val tempAndWind = getTempAndWind(citiesArray[i])
                newArrayList[i].temp = tempAndWind.first
                newArrayList[i].windSpeed = tempAndWind.second
                Log.d("test", newArrayList[i].toString())

            }
        }
        adapter.notifyDataSetChanged()
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SimpleFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
    fun getCityData() {
        adapter = RecyclerViewAdapter(newArrayList)
        newRecyclerView.adapter = adapter
        adapter.setOnItemClickListener(object: RecyclerViewAdapter.onItemClickListener{
            override fun onItemClick(position: Int) {
                val bundle = Bundle()
                bundle.putString("cityName", citiesArray[position].name)
                bundle.putSerializable("cityTemp", gson.toJson(newArrayList[position]))
                val fragment = DetailedFragment()
                fragment.arguments = bundle
                fragmentManager?.beginTransaction()?.replace(R.id.fragmentContainer, fragment)?.commit()

//                Toast.makeText(requireContext(), "You clicked on item no. $position", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun loadCities(): Array<City> {
        val gson = Gson()
        val cities_stream = resources.openRawResource(R.raw.cities)
        val citiesRaw = gson.fromJson(InputStreamReader(cities_stream), Cities::class.java).cities
        return citiesRaw
    }

    suspend fun getTempAndWind(city: City): Pair<Float, Float> {
        var temp = 0f
        var windSpeed = 0f
        try {

//            Log.d("test", "${city.capital}")

            val weatherURL = "https://api.openweathermap.org/data/2.5/weather?lat=${city.lat}&lon=${city.lon}&appid=$API_KEY&units=metric"
            val stream = URL(weatherURL).getContent() as InputStream
            val data = Scanner(stream).nextLine()
//            Log.d("test", data)
            val parser = JsonParser().parse(data).asJsonObject
            temp = parser.get("main").asJsonObject.get("temp").toString().toFloat()
//            Log.d("test", "${city.capital}, temp: $temp")
            windSpeed = parser.get("wind").asJsonObject.get("speed").toString().toFloat()
//            Log.d("test", "${city.capital}, temp: $windSpeed")
            return Pair(temp, windSpeed)
        } catch (e: Exception) {
            return Pair(0f, 0f)
        }


    }




}