package com.example.fragmentweather

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.fragmentweather.databinding.ActivityMainBinding
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import com.google.gson.JsonParser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DetailedFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailedFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        Log.d("test", "detailed start")
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_detailed, container, false)
        val cityNameView : TextView = view.findViewById(R.id.cityName)
        val cityTempView : TextView = view.findViewById(R.id.cityTemp)
        val cityWindView : TextView = view.findViewById(R.id.cityWindSpeed)
        val imgView : ImageView = view.findViewById(R.id.imageView)

        val args = this.arguments
        val cityTemp : CityTemp = Gson().fromJson(args?.getString("cityTemp"), CityTemp::class.java)

        cityNameView.setText(cityTemp.name)
        cityTempView.setText(cityTemp.temp?.toInt().toString())
        cityWindView.setText(cityTemp.windSpeed.toInt().toString())

        //https://api.teleport.org/api/urban_areas/slug:phoenix/images/

        GlobalScope.launch (Dispatchers.IO) {
//            val img_url = "https://d13k13wj6adfdf.cloudfront.net/urban_areas/phoenix-9fedc88f57.jpg"

//            Log.d("test", "get string url ${cityTemp.name}")

            val img_url = getStringUrl(cityTemp.name)
//            img_url = "https://d13k13wj6adfdf.cloudfront.net/urban_areas/phoenix-9fedc88f57.jpg"


            try {
                val img = getImage(img_url?.dropLast(1)?.drop(1))
                val background = BitmapDrawable(img)
                withContext(Dispatchers.Main) {
                    imgView.setImageDrawable(background)
                }

            } catch (e: Exception) {
            }
        }

        // Return to the first fragment
        val goBackButton : Button = view.findViewById<Button>(R.id.button)
        goBackButton.setOnClickListener {
            val fragment = SimpleFragment()
            fragmentManager?.beginTransaction()?.replace(R.id.fragmentContainer, fragment)?.commit()
        }


        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DetailedFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DetailedFragment().apply {

            }
    }

    suspend fun getStringUrl(cityName: String?) : String? {
        try {
            val path = "https://api.teleport.org/api/urban_areas/slug:${cityName?.toLowerCase()}/images/"
            Log.d("test", "path: $path")

            val stream = URL(path).getContent() as InputStream

            val data = Scanner(stream).nextLine()
            Log.d("test", data)
            val parser = JsonParser().parse(data).asJsonObject
            Log.d("test", "createdParser")
            Log.d("test", parser.get("photos").asJsonArray[0].asJsonObject.get("image").asJsonObject.get("web").toString())
            val urlString = parser.get("photos").asJsonArray[0].asJsonObject.get("image").asJsonObject.get("web").toString()

            Log.d("test", urlString)

            return urlString
        } catch (e: Exception) {
//            Toast.makeText(requireContext(), "Image not found", Toast.LENGTH_LONG).show()
//            Log.d("test", "failed getStringUrl")

        }

        return null
    }

    suspend fun getImage(url_string: String?) : Bitmap? {
        val url: URL = URL(url_string)
        val connection: HttpURLConnection?
        try {
            connection = url.openConnection() as HttpURLConnection
            connection.connect()
            val inputStream: InputStream = connection.inputStream
            val bufferedInputStream = BufferedInputStream(inputStream)
            return BitmapFactory.decodeStream(bufferedInputStream)
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(requireContext(), "Error", Toast.LENGTH_LONG).show()
        }
        return null

    }
}