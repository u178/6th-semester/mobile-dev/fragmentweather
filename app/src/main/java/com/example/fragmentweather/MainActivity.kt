package com.example.fragmentweather

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.fragmentweather.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

//    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer, SimpleFragment()).commit()

//        binding = ActivityMainBinding.inflate(layoutInflater)
//        setContentView(binding.root)

//        binding.simple.setOnClickListener {
//            replaceFragment(SimpleFragment())
//        }
//
//        binding.detailed.setOnClickListener {
//            replaceFragment(DetailedFragment())
//
//        }

    }

//    private fun replaceFragment(fragment: Fragment) {
//        val fragmentManager = supportFragmentManager
//        val fragmentTransaction = fragmentManager.beginTransaction()
//        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
//        fragmentTransaction.commit()
//    }
}