package com.example.currentweatherdatabinding

import com.example.fragmentweather.City
import com.google.gson.annotations.SerializedName

data class Cities(
    @SerializedName("cities") val cities: Array<City>
){
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Cities

        if (!cities.contentEquals(other.cities)) return false

        return true
    }

    override fun hashCode(): Int {
        return cities.contentHashCode()
    }
}
