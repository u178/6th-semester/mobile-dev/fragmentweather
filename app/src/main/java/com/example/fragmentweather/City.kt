package com.example.fragmentweather
import com.google.gson.annotations.SerializedName

data class City (
    @SerializedName("name") val name: String,
    @SerializedName("capital") val capital: String,
    @SerializedName("lat") val lat: Float,
    @SerializedName("lon") val lon: Float
){}
